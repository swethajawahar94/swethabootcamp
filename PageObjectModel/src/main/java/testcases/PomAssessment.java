package testcases;

import org.testng.annotations.Test;

import base.ProjectSpecificMethods;
import pages.LoginPage;
import pages.LoginPage1;


public class PomAssessment extends ProjectSpecificMethods {
	
	@Test
	public void runPomAssessment() throws InterruptedException
	{
		LoginPage1 lpn=new LoginPage1();
		lpn.enterUsername()
		.enterPassword()
		.clickLoginButton()
		.clickToggle()
		.clickViewAll()
		.clickOnServiceConsole()
		.selectDashboardfromDD()
		.clickOnNewDashbaord()
		.enterName()
		.enterDescription()
		.enterCreate()
		.clickOnDone()
		.clickOnSubscribe()
		.clickOnDaily()
		.clickOnTime().clickOnSave()
		.verifyCreatedDashboard()
		.closeTab()
		.clickOnPrivatedash()
        .verifyYourDashboardandDelete();		
	}

}
