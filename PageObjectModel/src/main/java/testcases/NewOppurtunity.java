package testcases;

import org.testng.annotations.Test;

import base.ProjectSpecificMethods;
import pages.LoginPage;

public class NewOppurtunity extends ProjectSpecificMethods{
	
	@Test
	public void runNewOppurtunity() throws InterruptedException
	{
	LoginPage lp= new LoginPage();
	lp.enterUsername().enterPassword().clickLoginButton().clickToggle().clickViewAll().clickOnSales().clickOppurtunity().clickNew().enterOppurtunityName().chooseCloseDate().chooseStage().clickOnSave();

	}
}
