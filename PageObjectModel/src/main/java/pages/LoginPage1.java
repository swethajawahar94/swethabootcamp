package pages;

import base.ProjectSpecificMethods;

public class LoginPage1 extends ProjectSpecificMethods {
	
	public LoginPage1 enterUsername()
	{
		driver.findElementById("username").sendKeys("makaia@testleaf.com");
		return this;
	}

	public LoginPage1 enterPassword()
	{
		driver.findElementById("password").sendKeys("SelBootcamp$1234");
		return this;
	}
	
	public HomePage1 clickLoginButton()
	{
		driver.findElementById("Login").click();
		return new HomePage1();
	}


}
