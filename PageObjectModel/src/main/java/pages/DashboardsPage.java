package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.ProjectSpecificMethods;

public class DashboardsPage extends ProjectSpecificMethods {

	public NewDashboard clickOnNewDashbaord()
	{
		driver.findElementByXPath("//div[text()='New Dashboard']").click();
		return new NewDashboard();
	}
	
	public DashboardsPage clickOnPrivatedash()
	{
		driver.findElementByXPath("//div[text()='Private Dashboard']").click();
		return this;
	}
	
	public DashboardsPage verifyYourDashboardandDelete() throws InterruptedException
	{
		List<WebElement> rows = driver.findElements(By.xpath("//table[@role='grid']/tbody/tr"));
		int rowCount=rows.size();
		for(int i=1;i<=rowCount;i++)
		{
			String name = driver.findElement(By.xpath("//table[@role='grid']/tbody/tr["+i+"]/th//span//lightning-formatted-url/a")).getText();
			if(name.equalsIgnoreCase("Swetha"))
			{
				System.out.println("Your dashboard is present");
				driver.findElement(By.xpath("//table[@role='grid']/tbody/tr["+i+"]/td[6]//span//lightning-primitive-cell-actions//button//lightning-primitive-icon")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath("//span[text()='Delete']")).click();
                driver.findElement(By.xpath("//span[text()='Delete'])[2]")).click();
                break;

			}
			
		}
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"))));
		System.out.println("Your dashboard got deleted successfully");
		return this;
		
	}

}
