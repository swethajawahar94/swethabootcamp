package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import base.ProjectSpecificMethods;

public class DashboardName extends ProjectSpecificMethods {
	
   public DashboardName clickOnDone()
   {
	driver.switchTo().defaultContent();
	WebDriverWait wait = new WebDriverWait(driver,30);
	wait.until(ExpectedConditions.titleContains("Swetha"));
	driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
	driver.findElement(By.xpath("//button[text()='Done']")).click();
	return this;
   }
   
   public DashboardName verifyCreatedDashboard()
   {
	   Assert.assertTrue(driver.findElement(By.xpath(("//span[text()='Swetha_Workout'])[3]"))).isDisplayed());
	   return this;
   }
   
   public DashboardName clickOnSubscribe()
   {
	   driver.findElement(By.xpath("//button[text()='Subscribe']")).click();
	   return this;
   }
 
   public DashboardName clickOnDaily()
   {
	   executor1.executeScript("arguments[0].click()", driver.findElement(By.xpath("//input[contains(@class,'uiInput uiInputRadio')]/following-sibling::span")));
	   return this;

   }
  
   public DashboardName clickOnTime()
   {
	   Select dd=new Select(driver.findElement(By.id("time")));
	   dd.selectByValue("10");
	   return this;

   }
  
   public DashboardName clickOnSave()
   {
	   driver.findElement(By.xpath("//span[text()='Save']")).click();
	   return this;

   }
   
   public DashboardName verifyToast()
   {
	   Assert.assertEquals(driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText(), "You started Dashboard Subscription");
	   return this;
   }
   
   public DashboardsPage closeTab()
   {
	   
	   driver.findElement(By.xpath("//button[@title='Close knS']//lightning-primitive-icon[1]")).click();
	   return new DashboardsPage();
   }
   
   
}
