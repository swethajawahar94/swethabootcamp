package pages;

import org.testng.Assert;

import base.ProjectSpecificMethods;

public class HomePage1  extends ProjectSpecificMethods {
	public HomePage1 VerifyHomePage()
	{
		Assert.assertEquals("Home|Salesforce", driver.getTitle());
		System.out.println("Landed in homepage");
		return this;
	}
	
	public HomePage1 clickToggle() throws InterruptedException
	{
		driver.findElementByClassName("slds-r5").click();
		return this;
	}
	
	public HomePage1 clickViewAll()
	{
		driver.findElementByXPath("//button[text()='View All']").click();
		return this;
	}
	
	public ServiceConsolePage clickOnServiceConsole()
	{
		driver.findElementByXPath("//p[text()='Service Console']").click();
		return new ServiceConsolePage();
	}


}
