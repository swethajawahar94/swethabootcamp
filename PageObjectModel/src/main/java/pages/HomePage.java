package pages;

import org.testng.Assert;

import base.ProjectSpecificMethods;

public class HomePage extends ProjectSpecificMethods{

	public HomePage VerifyHomePage()
	{
		Assert.assertEquals("Home|Salesforce", driver.getTitle());
		System.out.println("Landed in homepage");
		return this;
	}
	
	public HomePage clickToggle() throws InterruptedException
	{
		driver.findElementByClassName(prop.getProperty("HomePage.toggle.className")).click();
		return this;
	}
	
	public HomePage clickViewAll()
	{
		driver.findElementByXPath(prop.getProperty("HomePage.ViewAll.xpath")).click();
		return this;
	}
	
	public SalesPage clickOnSales()
	{
		driver.findElementByXPath(prop.getProperty("HomePage.Sales.xpath")).click();
		return new SalesPage();
	}
}
