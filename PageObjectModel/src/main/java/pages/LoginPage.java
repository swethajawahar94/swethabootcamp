package pages;

import base.ProjectSpecificMethods;

public class LoginPage extends ProjectSpecificMethods {
	
	public LoginPage enterUsername()
	{
		driver.findElementById(prop.getProperty("LoginPage.username.id")).sendKeys(prop.getProperty("username"));
		return this;
	}

	public LoginPage enterPassword()
	{
		driver.findElementById(prop.getProperty("LoginPage.password.id")).sendKeys(prop.getProperty("password"));
		return this;
	}
	
	public HomePage clickLoginButton()
	{
		driver.findElementById(prop.getProperty("LoginPage.LoginButton.id")).click();
		return new HomePage();
	}


}
