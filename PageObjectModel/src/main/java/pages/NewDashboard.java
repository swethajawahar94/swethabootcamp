package pages;

import org.openqa.selenium.By;

import base.ProjectSpecificMethods;

public class NewDashboard extends ProjectSpecificMethods {
	
	public NewDashboard enterName()
	{
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		driver.findElement(By.id("dashboardNameInput")).sendKeys("Swetha_Workout");
		return this;
	}
	
	public NewDashboard enterDescription()
	{
		driver.findElement(By.id("dashboardDescriptionInput")).sendKeys("testing");
		return this;
		
	}
	
	public DashboardName enterCreate()
	{
		driver.findElement(By.xpath("//button[text()='Create']")).click();
		return new DashboardName();
		 
	}
	

}
