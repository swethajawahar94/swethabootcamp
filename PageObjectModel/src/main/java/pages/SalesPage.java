package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import base.ProjectSpecificMethods;

public class SalesPage extends ProjectSpecificMethods {
	

	public OppurtunityPage clickOppurtunity()
	{
		WebElement opportunity = driver.findElementByXPath(prop.getProperty("SalesPage.clickOppurtunity.xpath"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", opportunity);
        return new OppurtunityPage();
		
	}

}
