package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import base.ProjectSpecificMethods;

public class OppurtunityPage extends ProjectSpecificMethods{
	
	public OppurtunityPage clickNew()
	{
		driver.findElementByXPath(prop.getProperty("OppurtunityPage.new.xpath")).click();		
		WebElement dropDown=driver.findElement(By.xpath(prop.getProperty("OppurtunityPage.dropdown.xpath")));
		executor1.executeScript("arguments[0].click();", dropDown);
		return this;

	}
	
	public OppurtunityPage enterOppurtunityName()
	{
		driver.findElementByXPath(prop.getProperty("OppurtunityPage.enterOppurtunityName.xpath")).sendKeys("Salesforce Automation by Swetha");
        return this;
	}
	
	public OppurtunityPage chooseCloseDate()
	{
		driver.findElementByXPath(prop.getProperty("OppurtunityPage.CloseDate.xpath")).sendKeys("13/11/2021");
		return this;
	}
	
	public OppurtunityPage chooseStage()
	{
		WebElement dropDown=driver.findElement(By.xpath(prop.getProperty("OppurtunityPage.Stage.xpath")));
		executor1.executeScript("arguments[0].click();", dropDown);
		driver.findElement(By.xpath(prop.getProperty("OppurtunityPage.NeedAnalysis.xpath"))).click();	
		return this;
	}
	
	public OppNamePage clickOnSave()
	{
		driver.findElement(By.xpath(prop.getProperty("OppurtunityPage.Save.xpath"))).click();
		return new OppNamePage();

	}

}
