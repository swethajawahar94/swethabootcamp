package pages;

import org.openqa.selenium.By;

import base.ProjectSpecificMethods;

public class ServiceConsolePage extends ProjectSpecificMethods {
	
	
	public DashboardsPage selectDashboardfromDD()
	{
		driver.findElementByXPath("//button[@title='Show Navigation Menu']/lightning-primitive-icon").click();
		executor1.executeScript("arguments[0].scrollIntoView()", driver.findElement(By.xpath("(//span[text()='Dashboards'])[2]")));
		executor1.executeScript("arguments[0].click()", driver.findElement(By.xpath("(//span[text()='Dashboards'])[2]")));
		return new DashboardsPage();
	}

	
}
