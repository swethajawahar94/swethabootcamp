package base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ProjectSpecificMethods {
        
	public static ChromeDriver driver;
    public JavascriptExecutor executor1 = (JavascriptExecutor) driver;
    public static Properties prop;

	@BeforeMethod
	public void LaunchApplication() throws IOException {
		

		FileInputStream fis= new FileInputStream("./src/main/resources/NewConfig.properties");
	
		prop=new Properties();
		
		prop.load(fis);
		
		ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        WebDriverManager.chromedriver().setup();
        driver= new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		
		driver.get("https://login.salesforce.com");
		
	}
	
//	@AfterMethod
//	public void closeBrowser()
//	{
//		driver.close();
//	}

}
