package week6.day1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class LearnToReadFromProperties {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
//		
		FileInputStream fis= new FileInputStream("./src/main/resources/NewConfig.properties");
	
		Properties prop=new Properties();
		
		prop.load(fis);
		
		System.out.println(prop.getProperty(""));

	}

}
